JupyterHub Server Image for KMC3-XPP
====================================

This is the official JupyterHub server image typically used at KMC3-XPP.
It starts off with a [scipy-image](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html)
and adds the follwing:

 - [nx5d](https://gitlab.com/codedump2/nx5d) from the `kmc3` branch, including its dependencies,
   most notably:
   - [xrayutitlities](https://github.com/dkriegner/xrayutilities),
   - [silx](https://github.com/silx-kit/silx),
   - [xarray](https://xarray.dev/),
   - [tifffile](https://github.com/cgohlke/tifffile),
 - [dask-labextenstion](https://github.com/dask/dask-labextension), a JupyterLab extension for Dask
   (the computer on which data analysis runs at KMC3 is quite beefy, and Dask is a straight-forward
   way to harness parallelism).
 - [Fisky](https://gitlab.com/kmc3-xpp/fisky), the [UDKM](https://www.uni-potsdam.de/en/udkm) Bluesky-based experimental orchestration setup. This makes the JHub image suitable to host a specific type of experiment at UDKM.
