FROM quay.io/jupyter/datascience-notebook:latest

USER root
RUN apt-get update && apt-get -y install graphviz htop iotop

USER jovyan
RUN pip install dask-labextension netcdf4 h5netcdf graphviz

RUN cd /tmp && \
    git clone --recurse-submodules https://gitlab.com/codedump2/nx5d -b kmc3 && \
    pip install /tmp/nx5d[xrd,kmc3,test]

RUN cd /tmp && \
    git clone --recurse-submodules https://gitlab.com/kmc3-xpp/fisky -b udkm && \
    pip install /tmp/fisky[test]

RUN cd /tmp && \
    git clone --recurse-submodules https://gitlab.com/kmc3-xpp/p3381_digest -b udkm && \
        pip install /tmp/p3381_digest

